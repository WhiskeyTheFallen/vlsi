-- DinoRun

-- Integrantes:
-- Gutiérrez Bañuelos Alan
-- Herrera Martínez Ulises
-- Palma Rodríguez Uzziel
-- Villegas Estrada Federico

library ieee;
use ieee.math_real.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity dinoRun is
port(	clk: in std_logic; -- Reloj de la tarjeta
		reset : in std_logic; -- Botón de reset
		botonIzquierdo: in std_logic; -- Botón para moverse a la izquiera
		botonDerecho: in std_logic; -- Botón para moverse a la izquiera
		hsync, vsync : out std_logic; -- Son para sincronizar el vga con el monitor
		VGA_R, VGA_G, VGA_B: out std_logic_vector(3 downto 0)); --Colores que se muestran en el monitor
end dinoRun;

architecture juego of dinoRun is

-- Señales de los divisores
signal div : std_logic := '0';
signal divVelocidad : std_logic := '0';

-- Señales de la posición 
signal posX : integer := 640;
signal posY : integer := 480;

-- Contador para la frecuencia de 1 Hz
signal contador: integer range 0 to 25000000;

-- Señal necesaria para saber si puede dibujar en la pantalla el vga
signal habilitado : std_logic;

-- Mapa de bits de la figura del dinosaurio

type matrizDino is array(0 to 39) of std_logic_vector(0 to 52);
signal dinosaurio: matrizDino := 
("01111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111000000000111",
"11111111111111111111111111111111111111000000000000000",
"11111111111111111111111111111111111111111000000011111",
"11111111111111111111111111111111111111110000110011111",
"11111111111111111111100000000000000000000000000011111",
"11111111111111111110000000000000000000000000000111111",
"11111111111111110000000000000000000000000011111111111",
"11111111111100000000000000000000000000011111111111111",
"11111000000000000000000000000000000000111111111111111",
"00000000000000000000000000000000000000111111111111111",
"11111111111111111100000000000000110010001111111111111",
"11111111111111111111110000001111110011110011111111111",
"11111111111111111111111000011111111101111011111111111",
"11111111111111111111111000111111111101111111111111111",
"11111111111111111111000001111111111111111111111111111",
"11111111111111111100110111111111111111111111111111111",
"11111111111111111011110111111111111111111111111111111",
"11111111111111110111110111111111111111111111111111111",
"11111111111111110111110111111111111111111111111111111",
"11111111111111110001110111111111111111111111111111111",
"11111111111111111111110001111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111");

-- Mapa de bits del meteorito

type matrizMeteorito is array(0 to 40) of std_logic_vector(0 to 52);
signal meteorito: matrizMeteorito := 
(
"01111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111111110111111111111111111111111111",
"11111111111111111111111110111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111110101111111111111111111111111111",
"11111111111111111111110001111111111111111111111111111",
"11111111111111111111110000001111111111111111111111111",
"11111111111111111111111000011111111111111111111111111",
"11111111111111111111111000011111111111111111111111111",
"11111111111111111111111000011111111111111111111111111",
"11111111111111111111000010011111111111111111111111111",
"11111111111111111111100011001011111111111111111111111",
"11111111111111111111100011000111111111111111111111111",
"11111111111111111111101011100111111111111111111111111",
"11111111111111111011101111110111111111111111111111111",
"11111111111111111000011111111110111111111111111111111",
"11111111111111111000111111111100111111111111111111111",
"11111111111111111101111111111110111111111111111111111",
"11111111111111111101111111111110111111111111111111111",
"11111111111111111111011111111101111111111111111111111",
"11111111111111111111101111111011111111111111111111111",
"11111111111111111111110111110111111111111111111111111",
"11111111111111111111111000001111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111",
"11111111111111111111111111111111111111111111111111111"
);

-- Matriz que representa la pantalla en una división de 12x12 elementos

type rom_type is array(0 to 11, 0 to 11) of integer;
signal matriz: rom_type := 
((0,0,0,0,0,0,0,0,0,0,0,0),
(0,0,0,0,0,0,0,0,0,0,0,0),
(0,0,0,0,0,0,0,0,0,0,0,0),
(0,0,0,0,0,0,0,0,0,0,0,0),
(0,0,0,0,0,0,0,0,0,0,0,0),
(0,0,0,0,0,0,0,0,0,0,0,0),
(0,0,0,0,0,0,0,0,0,0,0,0),
(0,0,0,0,0,0,0,0,0,0,0,0),
(0,0,0,0,0,0,0,0,0,0,0,0),
(0,0,0,0,0,0,0,0,0,0,0,0),
(0,0,0,0,0,0,0,0,0,0,0,0),
(0,0,0,0,0,2,0,0,0,0,0,0));

-- Matriz que guarda la posición de la matriz 'matriz' en donde aparecerán los meteoritos para el NIVEL 1

type secuencia1 is array (0 to 59, 0 to 2) of integer;
constant nivel1: secuencia1 :=
((7, 3, 11), (10, 0, 3), (11, 9, 6), (5, 3, 6), (10, 9, 5), (9, 4, 6),
(11, 6, 7), (2, 3 ,11), (1, 4, 5), (2, 9, 0), (3, 2, 5), (0, 3, 5),
(7, 5, 3), (9, 2 ,7), (4, 11, 10), (7, 10, 3), (0, 2, 4), (2, 10, 6),
(3, 11, 10), (1, 9, 7), (0, 3, 9), (10, 6, 11), (5, 11, 3), (0, 5, 10),
(0, 1, 11), (8, 3, 1), (10, 7, 3), (5, 4, 0), (5, 1, 8), (5, 11, 0),
(2, 11, 3), (3, 9, 8), (9, 7, 10), (9, 5, 3), (3, 5, 1), (3, 6, 8),
(6, 4, 0), (9, 5, 1), (8, 10, 6), (0, 4, 8), (7, 4, 0), (10, 1, 8),
(6, 0, 2), (6, 8 ,7), (0, 9, 10), (6, 8, 10), (5, 8, 1), (10, 2, 5),
(2, 0, 8), (4, 3,10), (2, 5, 3), (2, 4, 5), (2, 6, 7), (10, 6, 2),
(6, 0, 1), (0, 7, 9), (6, 9, 10), (3, 7, 9), (11, 1, 3), (5, 4, 3));

-- Matriz que guarda la posición de la matriz 'matriz' en donde aparecerán los meteoritos para el NIVEL 2

type secuencia2 is array (0 to 59, 0 to 3) of integer;
constant nivel2: secuencia2 :=
((1, 2, 4, 6), (0, 10, 3, 9), (11, 6, 7, 1), (2, 5, 6, 9), (8, 6, 7, 3), (8, 3, 5, 10),
(9, 6, 7, 5), (0, 4 ,9, 8), (7, 2, 0, 3), (2, 9, 6, 7), (10, 7, 5, 4), (2, 3, 4, 6),
(8, 3, 6, 11), (11, 9, 5 ,7), (8, 1, 9, 4), (7, 10, 3, 0), (9, 2, 8, 7), (2, 0, 9, 11),
(2, 5, 10, 9), (1, 2, 4 ,6), (0, 1, 9, 4), (5, 1, 9, 11), (8, 6, 3, 7), (2, 0, 11, 10),
(3, 8, 2, 9), (8, 3, 6 ,2), (10, 11, 6, 1), (8, 2, 7, 10), (10, 6, 7, 5), (9, 4, 8, 10),
(10, 11, 0, 6), (4, 11, 8 ,7), (9, 7, 11, 3), (11, 9, 2, 3), (7, 5, 10, 0), (11, 0, 7, 10),
(1, 5, 0, 9), (3, 9, 8 ,7), (4, 2, 6, 5), (0, 5, 8, 11), (7, 2, 8, 5), (2, 3, 7, 9),
(8, 1, 3, 7), (1, 9, 8 ,7), (4, 9, 10, 5), (0, 4, 8, 10), (6, 8, 10, 3), (7, 4, 6, 0),
(4, 5, 8, 6), (11, 3, 5 ,10), (11, 8, 4, 3), (3, 4, 1, 11), (10, 9, 11, 7), (0, 5, 7, 8),
(7, 5, 3, 9), (3, 10, 1 ,6), (11, 7, 2, 5), (8, 3, 7, 11), (11, 2, 7, 0), (9, 1, 6, 10));

-- Matriz que guarda la posición de la matriz 'matriz' en donde aparecerán los meteoritos para el NIVEL 3

type secuencia3 is array (0 to 59, 0 to 4) of integer;
constant nivel3: secuencia3 :=
((3, 5, 8, 1, 7), (9, 11, 0, 4, 10), (5, 7, 2, 8, 4), (1, 3, 6, 8, 11), (3, 4, 7, 10, 11), (2, 4, 7, 3, 9), 
(5, 7, 9, 0, 11), (7, 10, 4, 3, 2), (1, 2, 3, 7, 11), (10, 4, 6, 2, 1), (6, 5, 8, 10, 9), (1, 4, 7, 5, 10),
(3, 4, 2, 0, 10), (3, 4, 0, 11, 1), (3, 6, 7, 8, 9), (0, 3, 8, 9, 1), (2, 4, 5, 9, 0), (8, 3, 2, 0, 1),
(1, 9, 7, 6, 10), (8, 3, 1, 0, 5), (2, 7, 5, 0, 8), (9, 1, 10, 11, 6), (1, 4, 6, 2, 0), (1, 6, 8, 3, 10),
(11, 7, 6, 4, 5), (1, 10, 9, 2, 3), (3, 2, 4, 1, 0), (7, 5, 4, 3, 2), (1, 6, 3, 8, 3), (10, 11, 6, 3, 2),
(2, 1, 4, 6, 7), (2, 3, 7, 6, 10), (11, 3, 2, 5, 6), (1, 11, 5, 3, 2), (2, 3, 4, 7, 10), (11, 1, 0, 2, 3),
(0, 3, 4, 5, 2), (1, 3, 5, 4, 0), (7, 3, 2, 8, 1), (1, 5, 7, 10, 11), (7, 4, 5, 8, 10), (3, 4, 2, 1, 11),
(1, 3, 6, 7, 9), (10, 9, 11, 6, 7), (0, 2, 4, 3, 10), (11, 3, 5, 7, 2), (1, 0, 10, 11, 9), (8, 6, 5, 3, 2),
(8, 2, 4, 3, 1), (11, 3, 5, 0, 9), (7, 4, 3, 2, 11), (11, 0, 2, 5, 10), (7, 4, 3, 8, 1), (9, 7, 5, 8, 2),
(1, 3, 9, 11, 6), (7, 0, 2, 4, 10), (6, 5, 4, 2, 0), (10, 4, 6, 3, 2), (11, 2, 5, 3, 1), (10, 11, 4, 3, 1));

-- Entidad que tendrá como entradas el reloj de 50Mhz y de salida el sincronizador vertical y horizontal del VGA, la posición X y Y que actualizará 
-- a cada pixel de la pantalla de izquierda a derecha y de arriba hacia abajo

component syncVGA
port (clk : in std_logic;
		hsync, vsync, habilitado : out std_logic;
		posX : out integer range 0 to 800 := 0;
		posY : out integer range 0 to 525 := 0);
end component;

begin
	sincronizador: syncVGA port map (div, hsync, vsync, habilitado, posX, posY); -- Proceso para sincronizar el VGA con la tarjeta (se encuentra en el archiivo SyncVGA)

-- Proceso que divide a la mitad la frecuencia del reloj de 50 MHz a 25MHz para tener una resolucion de 640x480
process(clk) 
begin
	if rising_edge(clk) then
		div <= not div;
	end if;
end process;

-- Divisor de recuencia a 1 Hz
divisorVelocidadCaida: process(clk) --frec divisor = Reloj base(mclk) / N => N= (50MHz / 1 Hz)/2 -1 = 24999999
begin
	if rising_edge(clk) then
		if(contador=24999999) then -- el limite de cuenta para el divisor
			contador<=0;
			divVelocidad<=not divVelocidad;
		else 
			contador<=contador+1;
		end if;
	end if;
end process;

-- Proceso que se encarga de pintar la pantalla de acuerdo al número que contiene la matriz que la representa de 12x12.
pintar: process(div, divVelocidad)
	variable colorRGB: std_logic_vector(11	downto 0) := (others => '0'); -- variable que se encarga de manipular los colores RGB antes de proyectarlos en el monitor
	variable limiteX: integer range 0 to 11 := 0; -- Variable que almacena en qué cuadro se encuentra de la matriz 12x12 en el sentido horizontal
	variable limiteY: integer range 0 to 11 := 0; -- Variable que almacena en qué cuadro se encuentra de la matriz 12x12 en el sentido vertical
begin
	if rising_edge(div) then
		if habilitado = '1' then -- Si es habilitado quiere decir que es posible pintar en dicha zona
			if (posX > limiteX*53 AND posX < (limiteX*53)+53) then --Si se encuentra dentro de los límites verticales del cuadro 12-ésimo de la matriz
				if(posY >= limiteY*40 AND posY <= (limiteY*40)+40) then --Si se encuentra dentro de los límites horizontales del cuadro 12-ésimo de la matriz
					if matriz(limiteY, limiteX) = 1 then	-- Si hay un 1 en la matriz de reprresenta la pantalla
						if (meteorito(posY-(40*limiteY))(posX-(53*limiteX)) = '1') then	-- Dibuja el fondo del meteorito
							colorRGB := "0000"&"1000"&"1000";
						elsif (meteorito(0)(0) = '0') then -- Dibuja el meteorito
							colorRGB := "1001"&"0000"&"0000";
						end if;
					elsif matriz(limiteY, limiteX) = 2 then -- Si hay un 2 en la matriz de reprresenta la pantalla
						if (dinosaurio(posY-(40*limiteY))(posX-(53*limiteX)) = '1') then -- Dibuja el fondo del dinosaurio
							colorRGB := "0000"&"1000"&"1000";
						elsif (dinosaurio(0)(0) = '0') then -- Dibuja el cuerpo del dinosaurio
							colorRGB := "1111"&"1111"&"0000";
						end if;
					else
						colorRGB := "0000"&"1000"&"1000"; -- Dibuja el fondo del escenario
					end if;
         			 -- Se asignan los colores a las conexiones del VGA
					VGA_R <= colorRGB(11 downto 8);	
					VGA_G <= colorRGB(7 downto 4);
					VGA_B <= colorRGB(3 downto 0);
				else
					limiteY:=limiteY+1; --Si se pasa del límite vertical del cuadrado cambia al cuadrado inferior de la matriz 12x12
				end if;
			else
				limiteX:=limiteX+1; --Si se pasa del límite horizontal del cuadrado cambia al cuadrado inferior de la matriz 12x12
			end if;
		else
    		-- Se asignan los colores a las conexiones del VGA
			VGA_R <= "0000";
			VGA_G <= "0000";
			VGA_B <= "0000";
		end if;
	end if;
end process;

-- Proceso que se encarga de la lógica del juego
dinoRun: process(divVelocidad, botonIzquierdo, botonDerecho,reset)
	-- Variable que se encarga de mover la matriz de los niveles
	variable contador: integer range 0 to 71;
  	-- Variable que se encarga de contar en que nivel va y que confirma si ganó el juego
	variable nivel: integer range 1 to 4:=1;
  	-- Variable que se encarga de preguntar el botón para moverse a la izquierda se presionó
	variable moverIzquierda: std_logic;
  	-- Variable que se encarga de revisar si el jugador perdió
	variable perdedor: std_logic:='0';
  	-- Variable que se encarga de revisar si el botón reset se oprimió para luego modificar las variables dentro del if del divisor
	variable resetBandera: std_logic:='0';
begin
	-- Se pregunta si el reset no ha sido presionado, es lógica negada
	if reset = '1' then
		if rising_edge(divVelocidad) then
    		-- Se pregunta si el reset no se ha presionado, es necesario esta señal porque no se puede modificar la matriz fuera de la condicion de divVelocidad
			if (resetBandera = '0') then
				moverIzquierda := '0';
        		-- Se inicializa el primer renglón de la matriz en 0
				for i in 11 downto 0 loop
					matriz(0, i) <= 0;
				end loop;
        		-- Se pregunta si ya perdió el jugador para hacer la matriz a 1
				if (perdedor = '1') then
					for j in 11 downto 0 loop
						for i in 11 downto 0 loop
							matriz(j, i) <= 1;
						end loop;
					end loop;
        		-- También se pregunta si ganó para hacer la matriz a 2
				elsif (nivel = 4) then
					for j in 11 downto 0 loop
						for i in 11 downto 0 loop
							matriz(j, i) <= 2;
						end loop;
					end loop;
       			 -- En caso de que no haya ganado ni perdido, se hace la lógica del juego
				else
        			-- Se pregunta si no se ha acabado el nivel que son los 60 de la matriz más los 12 para que bajen
					if (contador < 71) then
          				-- Se pregunta si ya se paso por toda la matriz del nivel
						if (contador < 59) then 
            				-- Si es el nivel uno, se muestra un meteorito en las posiciones que están en la matriz de nivel1
							if (nivel=1) then
								for n in 0 to 2 loop
									matriz(0, nivel1(contador, n)) <= 1;
								end loop;
              				-- Si es el nivel dos, se muestra un meteorito en las posiciones que están en la matriz de nivel2
							elsif (nivel=2) then
								for n in 0 to 3 loop
									matriz(0, nivel2(contador, n)) <= 1;
								end loop;
              				-- Si es el nivel tres, se muestra un meteorito en las posiciones que están en la matriz de nivel3
							elsif (nivel=3) then
								for n in 0 to 4 loop
									matriz(0, nivel3(contador, n)) <= 1;
								end loop;
							end if;
						end if;
						contador:=contador+1;
          			-- Si ya se acabó un nivel aumentar el valor del nivel
					else
						contador:=0;
						nivel:=nivel+1;
					end if;
          
          			-- Se hace hace un for para recorrer toda la matriz, menos en el primer renglón, porque se modifica arriba
					for j in 11 downto 1 loop
						for i in 11 downto 0 loop
							if (j=11 AND matriz(11, i) = 2) then -- Se pregunta si es el jugador
								if (botonDerecho='0') then -- Se pregunta si se presionó el botón para mover el dinoaurio a la derecha
									if (i < 11) then -- Si el dinosaurio no se encuentra en el límite derecho se le deja mover a la derecha
										matriz(j, i+1)<=2;
										if (matriz(j-1, i)=1) then
											matriz(j, i)<=1; -- Si el cuadro de arriba es un meteorito entonces el metorito se desplaza hacia abajo
										else
											matriz(j, i)<=0; 
										end if;
										if (matriz(10, i+1)=1) then --Si el meteorito está a la derecha del dinosaurio entonces perdió
											perdedor:='1'; -- La bandera se hace 1 y acaba el juego.
										end if;
									else -- Si está en el extremo derecho el jugador ya no se mueve y solo se pregunta si arriba hay un meteorito
										if (matriz(10, i)=1) then
											perdedor:='1';
										end if;
									end if;
								elsif (botonIzquierdo='0') then -- Se pregunta si se presionó el botón para mover el dinoaurio a la izquierda
									moverIzquierda:='1';
									if (i > 0) then -- Si el dinosaurio se encuentra más allá del límite izquierdo
										matriz(j, i-1)<=2; -- Entonces se deja mover al dinosaurio
										if (matriz(j-1, i)=1) then -- Si arriiba de el hay un meteorito
											matriz(j, i)<=1; --Desaplazar el meteorito
										else
											matriz(j, i)<=0; --Si no hay un meteorito, desplazar el color del escenario.
										end if;
										if (matriz(10, i-1)=1) then --Si se dese mover el dinosaurio hacia un lugar en donde hay un meteorito
											perdedor:='1';-- Entonces se hace la bandera 1 y acaba el juego.
										end if;
									else
										if (matriz(10, i)=1) then --Si el dinosaurioo se dirije a la posiición de un meteorito
											perdedor:='1'; -- Entonces se hace la bandera 1 y acaba el juego.
										end if;
									end if;
								else -- Si no se movió el dinosaurio
									if (matriz(10, i)=1) then --Si en su posición hay un meteorito
										perdedor:='1'; -- Entonces se hace la bandera 1 y acaba el juego.
									end if;
								end if;
							else  -- Si el cuadro en el que se encuentra no es el dinosaurio
								if (i >= 0 AND i < 11) then --Y si se encuentra dentro de los límites de la matriz 12x12
									if ((matriz(j, i+1) = 2) AND (moverIzquierda = '1')) then --Si el cuadro de junto es el dinosaurio y no se apretó moverIzquerda
										moverIzquierda:='0'; --Activar mover izquerda
									else
										matriz(j, i)<=matriz(j-1, i); -- Desplazar el renglón de la matriz
									end if;
								else
									matriz(j, i)<=matriz(j-1, i);  -- Desplazar el renglón de la matriz
								end if;
							end if;
						end loop;
					end loop;
				end if;
      		-- Si se oprimió el botón de reset, que sería si la bandera se cambió, se ponen las variables a su estado original
			else
				resetBandera := '0';
				for j in 11 downto 0 loop
					for i in 11 downto 0 loop
						matriz(j, i) <= 0;
					end loop;
				end loop;
				matriz(11, 5)<= 2;
				perdedor:='0';
				contador:=0;
				nivel:=1;
			end if;
		end if;
  	-- Si se presionó el reset se cambía el valor de la bandera
	else
		resetBandera := '1';
	end if;
end process;

end juego;