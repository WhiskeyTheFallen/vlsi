-- Proceso para hacer la sincornización del VGA con el monitor

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--Entidad que tiene como entradas el reloj, de salidas si está habilitada la posición, las sincronizaciones horizontal y vertical, así como las posiciones X y Y.
entity syncVGA is
port (clk : in std_logic;
		hsync, vsync, habilitado : out std_logic;
		posX : out integer range 0 to 800 := 0;
		posY : out integer range 0 to 525 := 0);
end syncVGA;

architecture vga of syncVGA is

--Señal que contiene la posición en x

signal x_pos : integer range 0 to 800 := 0;

--Señal que contiene la posición en y

signal y_pos : integer range 0 to 525 := 0;

begin
	process(clk)
	begin
		if rising_edge(clk) then 
			if x_pos < 800 then --Si está dentro del límite disponible en x
				x_pos <= x_pos + 1; --Permite el desplazamiento en x
			else
				x_pos <= 0; -- Se reinicia la posición en x
				if y_pos < 525 then --Si está dentro del límite disponible en y
					y_pos <= y_pos + 1; --Permite el desplazamiento en y
				else
					y_pos <= 0; --En caso contrario regresa a la posición 0 en y
				end if;
			end if;
			
      		-- Se pregunta si está en la zona para el horizontal sync
			if (x_pos > 655 and x_pos < 752) then
				hsync <= '0';
			else
				hsync <= '1';
			end if;
			
     		-- Se pregunta si está en la zona para el vertical sync
			if (y_pos > 489 and y_pos < 492) then
				vsync <= '0';
			else
				vsync <= '1';
			end if;
			
      		-- Se pone habilitado en '1' cuando está en la zona en donde se permite dibujar
			if (x_pos < 640 and y_pos < 480) then
				habilitado <= '1';
			else
				habilitado <= '0';
			end if;
			
      		-- Se asignan los valores de las posiciones para que las tenga la entidad principal
			posX <= x_pos;
			posY <= y_pos;
      
		end if;
	end process;
end vga;