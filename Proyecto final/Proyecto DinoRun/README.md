Integrantes:

Gutiérrez Bañuelos Alan

Herrera Martínez Ulises

Palma Rodríguez Uzziel

Villegas Estrada Federico


Descripción:
Es un juego que se muestra en un monitor con una conexión VGA, el cual consiste en un dinosaurio tratando de esquivar meteoritos, 
existen 3 niveles, en el primer nivel habrán 3 meteoritos cayendo a la vez, en el segundo nivel 4 meteoritos y en el tercer nivel 5 meteoritos, 
si el dinosaurio no ha muerto finalizando los 3 niveles habrá ganado el juego.


Video:

https://youtu.be/Rb4SUnlRkx0
