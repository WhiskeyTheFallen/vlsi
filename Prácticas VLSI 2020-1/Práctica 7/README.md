Integrantes:

Gutiérrez Bañuelos Alan

Herrera Martínez Ulises

Palma Rodríguez Uzziel

Villegas Estrada Federico


Descripción:

Observar las diferencias entre el método de direccionamiento entrada-estado y 
el método de direccionamiento por trayectoria, comparando su comportamiento, sus carta ASM
y sus diferencias en el reporte de síntesis.


Video:

https://youtu.be/n9GwGCd4QVs