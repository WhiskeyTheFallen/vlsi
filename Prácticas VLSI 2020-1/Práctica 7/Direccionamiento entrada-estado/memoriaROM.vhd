
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity memoriaRom is
generic(
	--Descripci�n de la esctructura l�gica de la memoria ROM
	addr_width: integer:=8; --N�mero de localidades
	addr_bits: integer:=3;
	data_width: integer:=14);
	--Descripci�n de la esctructura f�sica de la memoria ROM
Port ( addr:	in  STD_LOGIC_VECTOR(addr_bits - 1 downto 0);
       data:	out  STD_LOGIC_VECTOR(data_width - 1 downto 0));
  
end memoriaRom;

architecture Behavioral of memoriaRom is
type rom_type is array(0 to addr_width - 1) of
STD_LOGIC_VECTOR(data_width - 1 downto 0);
-- Estas son las salidas que obtuvimos de la carta ASM
signal edossalids: rom_type := (
					"01101110000110",
					"11010010100100",
					"10111100011000",
					"00010100001000",
					"11000000010100",
					"11001001010000",
					"11011011001000",
					"11001001000001");
					
begin
-- Se asigna a data la salida dependiendo de la direcci�n que queremos
data <= edossalids(conv_integer(unsigned(addr)));
end Behavioral;
