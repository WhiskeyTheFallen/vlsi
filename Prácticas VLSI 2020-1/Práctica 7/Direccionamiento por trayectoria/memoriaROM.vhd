
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity memoriaRom is
generic(
	--Descripción de la esctructura lógica de la memoria ROM
	addr_width: integer:=40; --Número de localidades
	addr_bits: integer:=6;
	data_width: integer:=9);
	--Descripción de la esctructura física de la memoria ROM
Port ( addr:	in  STD_LOGIC_VECTOR(addr_bits - 1 downto 0);
       data:	out  STD_LOGIC_VECTOR(data_width - 1 downto 0));
  
end memoriaRom;

architecture Behavioral of memoriaRom is
type rom_type is array(0 to addr_width - 1) of
STD_LOGIC_VECTOR(data_width - 1 downto 0);
-- Estas son las salidas que obtuvimos de la carta ASM
signal edossalids: rom_type := (
					"001011010",
					"001011010",
					"011011100",
					"011011100",
					"001011010",
					"001011010",
					"011011100",
					"011011100",
					"010001001",
					"010001001",
					"010001001",
					"010001001",
					"010001001",
					"010001001",
					"010001001",
					"010001001",
					"001100110",
					"100000110",
					"001100110",
					"100000110",
					"001100110",
					"100000110",
					"001100110",
					"100000110",
					"010000100",
					"010000100",
					"010000100",
					"010000100",
					"100000100",
					"100000100",
					"100000100",
					"100000100",
					"000001010",
					"000001010",
					"000001010",
					"000001010",
					"000001010",
					"000001010",
					"000001010",
					"000001010");
					
begin
-- Se asigna a data la salida dependiendo de la dirección que queremos
data <= edossalids(conv_integer(unsigned(addr)));
end Behavioral;
