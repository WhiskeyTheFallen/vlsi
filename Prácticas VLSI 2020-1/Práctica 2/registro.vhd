library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity registro is
    Port ( mclk : in  STD_LOGIC;
				reset: in STD_LOGIC;
           LEDS : out STD_LOGIC_VECTOR (7 downto 0));
end registro;

architecture Behavioral of registro is
signal delay: integer range 0 to 64000000:=0; -- Reloj base 64 MHz
signal div: std_logic:='0';
signal conta: integer range 0 to 15:=0; --Hacemos uso de un contador para regular la el corrimiento de los LEDS
signal corrimiento: STD_LOGIC_VECTOR (7 downto 0):="00000001"; --Iniciamos el corrimiento en el LED menos significativo ( el m�s abajo en el caso de nuestra tarjeta)

begin

--divisor @1 Hz
divisor: process(mclk) --frec divisor = Reloj base(mclk) / N => N= (64MHz / 1 Hz)/2 -1 = 31999999
begin
		if rising_edge(mclk) then
			if(delay= 31999999) then -- el limite de cuenta para el DIVISOR es N
				delay<=0;
				div<=not div;
			else 
			   delay<=delay+1;
			end if;
		end if;
end process;

registro: process (div,reset)
	begin
		if(reset = '0') then --Verificamos si se ha presionado el bot�n de reinicio
			if(rising_edge(div)) then
				if (conta < 7) then --Preguntamos que no haya llegado a�n al LED m�s significativo, haciendo uso de nuestro contador.
					corrimiento(1) <= corrimiento(0);
					corrimiento(2) <= corrimiento(1);
					corrimiento(3) <= corrimiento(2);
					corrimiento(4) <= corrimiento(3);
					corrimiento(5) <= corrimiento(4);
					corrimiento(6) <= corrimiento(5);
					corrimiento(7) <= corrimiento(6);
					corrimiento(0) <= corrimiento(7);
				else --En caso de que haya llegado a este, entonces invertimos el corrimiento
					corrimiento(6) <= corrimiento(7);
					corrimiento(5) <= corrimiento(6);
					corrimiento(4) <= corrimiento(5);
					corrimiento(3) <= corrimiento(4);
					corrimiento(2) <= corrimiento(3);
					corrimiento(1) <= corrimiento(2);
					corrimiento(0) <= corrimiento(1);
					corrimiento(7) <= corrimiento(0);
				end if;
				if(conta = 13) then
					conta <= 0;
				else
					conta <= conta+1;
				end if;
				
			end if;
		else --Funcionamiento del reset, se pone el contador a cero y el corrimiento se pone a su estado inicial
			conta <= 0;
			corrimiento <= "00000001";
		end if;
				
		
end process;

LEDS<=not(corrimiento); -- NOT porque los leds estan conectados en l�gica negada

end Behavioral;

