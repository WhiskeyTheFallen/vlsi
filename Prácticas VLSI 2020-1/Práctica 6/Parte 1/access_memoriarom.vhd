
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity access_memoriaROM is
Port ( sw:	in  STD_LOGIC_VECTOR(3 downto 0); -- variable switch
       disp:	out  STD_LOGIC_VECTOR(6 downto 0)); -- variable display
end access_memoriaROM;

architecture Behavioral of access_memoriaROM is
signal data : STD_LOGIC_VECTOR(6 downto 0);
begin
 
seg_ROM_7: entity work.memoriaROM
Port map (addr=>sw, data=>data);--Mapeamos los puertos de los dos módulos
disp <= data;
end Behavioral;