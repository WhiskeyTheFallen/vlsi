Integrantes:

Gutiérrez Bañuelos Alan

Herrera Martínez Ulises

Palma Rodríguez Uzziel

Villegas Estrada Federico


Descripción:

Parte 1:

Se desea a partir de un DIP switch ingresar a una memoria, a partir de la combinación de estos
se ingresará a un cierto registro para mostrar su valor, el contenido de la memoria son los números
hexadecimales del 0 - F.

Parte 2:

Se desea ingresar a los registros de una memoria de forma automática,
el contenido de la memoria son los números hexadecimales del 0 - F.

Parte 3:

Se desea ingresar a los registros de una memoria de forma automática,
el contenido de la memoria es VLSI-2020_1-UAUF.

Video:

https://youtu.be/azbPtoHqHXE
