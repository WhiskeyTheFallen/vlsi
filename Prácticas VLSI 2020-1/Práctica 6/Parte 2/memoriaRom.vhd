

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity memoriaRom is
generic(
	--Descripción de la esctructura lógica de la memoria ROM
	addr_width: integer:=16; --Número de localidades
	addr_bits: integer:=4;
	data_width: integer:=7);
	--Descripción de la esctructura física de la memoria ROM
Port ( addr:	in  STD_LOGIC_VECTOR(addr_bits - 1 downto 0);
       data:	out  STD_LOGIC_VECTOR(data_width - 1 downto 0));
  
end memoriaRom;

architecture Behavioral of memoriaRom is
type rom_type is array(0 to addr_width - 1) of
STD_LOGIC_VECTOR(data_width - 1 downto 0);
signal seg7: rom_type := (
					"1111110", -- 0
					"0110000", -- 1
					"1101101", -- 2
					"1111001", -- 3
					"0110011", -- 4
					"1011011", -- 5
					"1011111", -- 6
					"1110000", -- 7
					"1111111", -- 8
					"1110011", -- 9
					"1110111", -- A
					"0011111", -- b
					"0001101", -- c
					"0111101", -- d
					"1001111", -- E
					"1000111");-- F
					
begin
data <= seg7(conv_integer(unsigned(addr)));
end Behavioral;
