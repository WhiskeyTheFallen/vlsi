
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity access_memoriaROM is
Port ( mclk: in  STD_LOGIC; -- reloj
       disp:	out  STD_LOGIC_VECTOR(6 downto 0));
end access_memoriaROM;

architecture Behavioral of access_memoriaROM is
signal data : STD_LOGIC_VECTOR(6 downto 0);
signal conta : STD_LOGIC_VECTOR(3 downto 0):="0000";
signal delay: integer range 0 to 64000000:=0; -- reloj base 64 MHz
signal div: std_logic:='0';

begin

-- Divisor para la entrada t con periodo de un segundo
divisor: process(mclk) -- frec divisor = Reloj base(mclk) / N => N= (64MHz / 1 Hz)/2 -1 = 31999999 
	begin
		if rising_edge(mclk) then
			if(delay=31999999) then -- el limite de cuenta para el DIVISOR es N
				delay<=0; -- se reinicia el conteo
				div <= not(div);
			else
				delay<=delay+1;
			end if;
		end if;
end process;

-- Cada segundo se incrementa en uno la posici�n del registro que se est� mostrando
contador: process(div) 
	begin
		if rising_edge(div) then
			if(conta="1111") then
				conta<="0000";
			else
				conta<=conta+1;
			end if;
		end if;
end process;

seg_ROM_7: entity work.memoriaROM

Port map (addr=>conta, data=>data);--Mapeamos los puertos de los dos m�dulos
disp <= data;
end Behavioral;

