library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity led is
    Port ( mclk	: in  STD_LOGIC;
			  switch:	in  STD_LOGIC_VECTOR(3 downto 0);
           ledout: 		out STD_LOGIC);
end led;

architecture Behavioral of led is

signal delay: integer range 0 to 640000:=0; -- Reloj de 64 MHz/100Hz
signal porcentaje : integer;


begin


porcentaje <= conv_integer(unsigned(switch)); -- se convierte a entero la combinaci�n binaria metida por el deep switch

porcentaje <= '10' when (procentaje > 10) else porcentaje; -- solo hace caso a los primero 10 n�meros binarios de switch

led: process(mclk)
begin
	if rising_edge(mclk) then
			if(delay<= (porcentaje * 640000)/10) then -- se multiplica la frecuencia por el factor para determinar el tiempo en alto
				ledout <= '1';
			else
				ledout <= '0';
			end if;
			delay<=delay+1;	-- se hace el incremento al contador
	end if;
end process;

end Behavioral;