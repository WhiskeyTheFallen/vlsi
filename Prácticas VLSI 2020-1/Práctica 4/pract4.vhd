
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity pract4 is
Port ( 	mclk: in  STD_LOGIC; -- reloj
				sd, si: in  STD_LOGIC; -- sensor de entrada
				reset: in  STD_LOGIC:='0'; -- bot�n de reset
				display: out STD_LOGIC_VECTOR(6 downto 0); -- display para los estados
				direcciones: out STD_LOGIC_VECTOR(3 downto 0):="1111");
				-- adelante, atras, giro_der, giro_izq bombas de salida
end pract4;

architecture Behavioral of pract4 is
type estado is (E0, E1, E2, E3, E4, E5, E6, E7, E8, E9, E10, E11); -- codificacion de estados
signal qt:  estado := E0; -- variable para guardar el estado en el que se encuentra el sistema
signal delay: integer range 0 to 64000000:=0; -- reloj base 64 MHz
signal div: std_logic:='0';

begin 

-- Divisor para la entrada t con periodo de un segundo
divisor: process(mclk) -- frec divisor = Reloj base(mclk) / N => N= (64MHz / 1 Hz)/2 -1 = 31999999 
	begin
		if rising_edge(mclk) then
			if(delay=31999999) then -- el limite de cuenta para el DIVISOR es N
				delay<=0; -- se reinicia el conteo
				div <= not(div);
			else
				delay<=delay+1;
			end if;
		end if;
end process;

sistema: process(div,reset) -- proceso para mover de estado y asignar salidas  
	begin
	if(reset='1') then
		qt <= E0;
	elsif rising_edge(div) then
		case qt is 
			when E0 => -- qt pasa al estado E0
				display <= "0111111"; -- se muestra un 0
				if (si = '0' and sd = '0') then
					direcciones <= "0111";
				elsif (si = '0' and sd = '1') then
					qt <= E1;
				elsif (si = '1' and sd = '0') then
					qt <= E3;
				else 
					qt <= E5;
				end if;
			when E1 => -- qt pasa al estado E1
				display <= "0000110"; -- se muestra un 1
				direcciones <= "1011";
				qt<= E2;
			when E2 => -- qt pasa al estado E2
				display <= "1011011"; -- se muestra un 2
				direcciones <= "1110";
				qt<= E0;
			when E3 => -- qt pasa al estado E3
				display <= "1001111"; -- se muestra un 3
				direcciones <= "1011";
				qt<= E4;
			when E4 => -- qt pasa al estado E4
				display <= "1100110"; -- se muestra un 4
				direcciones <= "1101";
				qt <=E0;
			when E5 => -- qt pasa al estado E5
				display <= "1101101"; -- se muestra un 5
				direcciones <= "1011";
				qt<= E6;
			when E6 => -- qt pasa al estado E6
				display <= "1111101"; -- se muestra un 6
				direcciones <= "1110";
				qt<= E7;
			when E7 => -- qt pasa al estado E7
				display <= "0000111"; -- se muestra un 7
				direcciones <= "1110";
				qt<= E8;
			when E8 => -- qt pasa al estado E8
				display <= "1111111"; -- se muestra un 8
				direcciones <= "0111";
				qt<= E9;
			when E9 => -- qt pasa al estado E9
				display <= "1100111"; -- se muestra un 9
				direcciones <= "0111";
				qt<= E10;
			when E10 => -- qt pasa al estado E10
				display <= "1110111"; -- se muestra un A
				direcciones <= "1101";
				qt<= E11;
			when E11 => -- qt pasa al estado E11
				display <= "1111100"; -- se muestra un b
				direcciones <= "1101";
				qt<= E0;
		end case;
	end if;
end process;

end Behavioral;

