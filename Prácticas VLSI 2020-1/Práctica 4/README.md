Integrantes:

Gutiérrez Bañuelos Alan

Herrera Martínez Ulises

Palma Rodríguez Uzziel

Villegas Estrada Federico


Descripción:

Se requiere diseñar el sistema de control de vuelo de un UAV. El vehículo cuenta con una unidad
de medición inercial equipada con dos sensores, uno para el hemisferio derecho (Sd) y otro para el
izquierdo (Si), con ambos el UAV deberá tomar la decisión sobre qué movimiento deberá efectuar, los
cuales son: "ADELANTE", "ATRAS", "GIRO_IZQ", y "GIRO_DER".


Video:

https://www.youtube.com/watch?v=LOIU5PHzXYk
