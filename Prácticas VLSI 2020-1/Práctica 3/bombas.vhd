library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity bombas is
    Port ( 	mclk: in  STD_LOGIC;		-- reloj
				H: in  STD_LOGIC;			-- sensor de entrada
				A, B: out  STD_LOGIC);	-- bombas de salida
end bombas;

	architecture Behavioral of bombas is
	type estado is (E0, E1, E2, E3); -- Codificacion de estados
	signal qt:  estado; 					-- Estado siguiente

begin

sistema: process(mclk) 					-- Proceso para mover de estado y asignar salidas  
	begin
		if rising_edge(mclk) then		-- Se pregunta por el flanco de subida del reloj
			case qt is when E0 =>		-- qt pasa al estado E0
				A<=not('0');				-- Se asigna la salida con logica negada 
				B<=not('0');				-- Se asigna la salida con logica negada 
				if(H='0') then 
					qt<=E1;
				else
					qt<=E0;
				end if;
				when E1 =>					-- qt pasa al estado E1
				A<=not('1');				-- Se asigna la salida con logica negada 
				B<=not('0');				-- Se asigna la salida con logica negada 
				if(H='0') then 
					qt<=E1;
				else
					qt<=E2;
				end if;
				when E2 =>					-- qt pasa al estado E2
				A<=not('0');				-- Se asigna la salida con logica negada 
				B<=not('0');				-- Se asigna la salida con logica negada 
				if(H='0') then 
					qt<=E3;
				else
					qt<=E2;
				end if;
				when E3 =>					-- qt pasa al estado E3
				A<=not('0');				-- Se asigna la salida con logica negada 
				B<=not('1');				-- Se asigna la salida con logica negada 
				if(H='0') then 
					qt<=E3;
				else
					qt<=E0;
				end if;
			end case;
		end if;
end process;

end Behavioral;
